#!/usr/bin/env bash

for dir in images/*/; do
  echo "checking $dir"
  if [[ $FORCE_ALL == "true" || -n $(git diff --name-only HEAD~1..HEAD -- "$dir") ]]; then
    IMAGE_NAME="qaack/$(echo "$dir" | cut -d "/" -f2)"        
    docker build --no-cache -t "$IMAGE_NAME" "$dir"
    docker push $IMAGE_NAME
  else
    echo "No changes in $dir, skipping build"
  fi
done
